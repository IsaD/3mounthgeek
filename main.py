import os
from aiogram import Bot, Dispatcher, types, executor
from dotenv import load_dotenv
from os import getenv
import random

load_dotenv()
TOKEN = os.getenv("TOKEN")
bot = Bot(TOKEN)

dp = Dispatcher(bot=bot)
@dp.message_handler(commands=["start"])
async def start(message: types.Message):
    await message.answer(f"Hello {message.from_user.username}")

@dp.message_handler(commands=['myinfo'])
async def myinfo_command(message: types.Message):
    user_id = message.from_user.id
    first_name = message.from_user.first_name
    username = message.from_user.username

    info_text = f"Ваш id: {user_id}\nВаше имя: {first_name}\nВаше имя пользователя: {username}"
    await message.reply(info_text)

@dp.message_handler(commands=['picture'])
async def picture_command(message: types.Message):
    images_folder = "images"
    image_files = os.listdir(images_folder)
    random_image = random.choice(image_files)
    image_path = os.path.join(images_folder, random_image)

    with open(image_path, "rb") as file:
        await bot.send_photo(chat_id=message.chat.id, photo=file)

if __name__ == "__main__":
    executor.start_polling(dp)